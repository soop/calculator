/**
 * a calculator with history mode.
 */


const buttons = document.querySelectorAll('.btn');

buttons.forEach(element => {
  element.addEventListener('click', () => {
    let value = element.getAttribute('id');

    switch (value) {
      default:
      case '1':
        calc.addToEquation(value);
        break;
      case 'clear':
        calc.clearDisplay();
        break;
      case 'equal':
        calc.solveEquation();
        break;
    }
  });
});


class calculator {
  constructor() {
    this.history = [],
      this.currentInput = '',
      this.latestSolve = ''
  }

  updateDisplay() {
    document.querySelector('.display').value = this.currentInput;
  }

  updateSolveDisplay() {
    document.querySelector('.result-display').innerHTML = `= ${this.latestSolve}`;
  }

  clearDisplay() {
    this.history.push(this.currentInput);

    this.currentInput = '';
    this.updateDisplay();
  }

  addToEquation(param) {
    this.currentInput += param;

    this.updateDisplay();
  }

  solveEquation() {
    this.latestSolve = eval(this.currentInput);

    this.updateSolveDisplay();
  }
}


let calc = new calculator();



setTimeout(() => {
  calc.clearDisplay();
},200)
